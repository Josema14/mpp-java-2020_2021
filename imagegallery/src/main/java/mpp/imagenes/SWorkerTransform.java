package mpp.imagenes;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.ExecutionException;

import javax.imageio.ImageIO;
import javax.swing.SwingWorker;

import mpp.imagenes.db.LabelledImage;

public class SWorkerTransform extends SwingWorker<String, Image> {
	
	File folder;
	LabelledImage i;
	Controller c;

	public SWorkerTransform(File f,LabelledImage i, Controller controller) {

		this.folder = f;
		this.i = i;
		this.c = controller;

	
	}


	@Override
	protected String doInBackground() throws Exception {
		File f = i.getFile();
		try {
			BufferedImage image = ImageIO.read(f);
			float[] blurMatrix = { 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f,
					1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f };
			BufferedImageOp blurFilter = new ConvolveOp(new Kernel(3, 3, blurMatrix), ConvolveOp.EDGE_NO_OP, null);

			BufferedImage filtered = blurFilter.filter(image, null);

			File outputFileName = Paths.get(folder.getPath(), f.getName()).toFile();
			ImageIO.write(filtered, "jpg", outputFileName);
			return f.getName();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
	
	@Override
	public void done() {

		try {
			c.setInfo(get(),100);

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
