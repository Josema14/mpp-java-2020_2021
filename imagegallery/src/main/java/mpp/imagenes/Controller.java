package mpp.imagenes;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.imageio.ImageIO;
import javax.swing.SwingWorker;

import mpp.imagenes.db.ImageDB;
import mpp.imagenes.db.LabelledImage;
import mpp.imagenes.labels.LabelDetector;
import mpp.imagenes.labels.LabelDetector.Label;
import mpp.imagenes.labels.MockLabelDetector;

/**
 * 
 * @author jesus
 *
 */
public class Controller {

	private static int MAX_THREADS = 8; // Maximum number of threads

	@Nonnull
	private final ImageDB db;
	@Nonnull
	private final ImageManager view;
	@Nonnull
	private final LabelDetector labelDetector = new MockLabelDetector();
	@CheckForNull
	private LabelledImage currentImage;

	// threadPool para Transformación y Carga de Imagenes
	private ExecutorService threadPool;

	public Controller(@Nonnull ImageManager view) {
		this.view = view;
		this.db = new ImageDB();
	}

	public LabelledImage setCurrentImage(File file) {
		LabelledImage img = db.getImage(file);
		if (img == null)
			return null;
		this.currentImage = img;
		return img;
	}

	public LabelledImage getCurrentImage() {
		return currentImage;
	}

	public void loadScaledImage(@Nonnull File f, @Nonnegative int width, @Nonnegative int height) throws IOException {
		SWorkerLoadImage hilo = new SWorkerLoadImage(f, width, height, this, 1);
		hilo.execute();

	}

	public void addImagen(File f, Image imagen, int count) {

		db.addImage(f);
		view.addThumbnail(imagen, f);
		view.setInfo(f.getName() + " terminado", view.getInfo() + 100 / count);

	}

	public void loadScaledImages(@Nonnull List<? extends File> files, @Nonnegative int width, @Nonnegative int height)
			throws IOException {
		// Inicializamos el threadPool por si se ha cancelado la operación anterior
		threadPool = Executors.newFixedThreadPool(MAX_THREADS);
		for (File file : files) {

			SWorkerLoadImage hilo = new SWorkerLoadImage(file, width, height, this, files.size());
			threadPool.submit(hilo);

		}

	}

	// http://www.java2s.com/Code/Java/2D-Graphics-GUI/ImageFilter.htm
	public void transformAll(@Nonnull File folder) {

		// Inicializamos el threadPool por si se ha cancelado la operación anterior
		threadPool = Executors.newFixedThreadPool(MAX_THREADS);

		for (LabelledImage labelledImage : db.getImages()) {
			// Creamos un SwingWorker que se encargue de transformar los archivos
			// Usar ThreadPool para limitar hilos

			SWorkerTransform hilo = new SWorkerTransform(folder, labelledImage, this);
			threadPool.submit(hilo);

		}
	}

	private void transformFile(File folder, LabelledImage labelledImage) {
		File f = labelledImage.getFile();
		try {
			BufferedImage image = ImageIO.read(f);
			float[] blurMatrix = { 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f,
					1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f };
			BufferedImageOp blurFilter = new ConvolveOp(new Kernel(3, 3, blurMatrix), ConvolveOp.EDGE_NO_OP, null);

			BufferedImage filtered = blurFilter.filter(image, null);

			File outputFileName = Paths.get(folder.getPath(), f.getName()).toFile();
			ImageIO.write(filtered, "jpg", outputFileName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@CheckForNull
	protected Image loadScaled(@Nonnull File f, @Nonnegative int w, @Nonnegative int h) throws IOException {
		System.out.println("Processing: " + f);
		BufferedImage img = ImageIO.read(f);
		if (img == null)
			return null;
		return img.getScaledInstance(w, h, Image.SCALE_FAST);
	}

	public void stopBackgroundTasks() {

		if (threadPool != null) {
			// Cancelamos cualquier operación en curso
			threadPool.shutdownNow();
			//Arreglar Barra de progreso
			if (threadPool.isTerminated()) {
			
				
					setInfo("Operacion cancelada", 0);
				
				
			}
		}

	}

	public void autoLabel() {
		for (LabelledImage img : db.getImages()) {
			try {
				List<Label> labels = labelDetector.label(img.getFile());
				for (Label label : labels) {
					img.addLabel(label.getValue());
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void setInfo(String string, int i) {
		view.setInfo(string, i);
	}

}
