package mpp.imagenes;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import mpp.imagenes.db.ImageDB;

public class SWorkerLoadImage extends SwingWorker<Image, Image> {

	private File f;
	private int width;
	private int height;
	private Controller controller;
	private boolean imagenNula = false;
	private boolean cancelado = false;
	private int numImagenes = 0;


	public SWorkerLoadImage(File f, int width, int height, Controller view, int numImagenes) {

		this.f = f;
		this.width = width;
		this.height = height;
		this.controller = view;
		this.numImagenes = numImagenes;

	
	}

	@CheckForNull
	protected Image loadScaled(@Nonnull File f, @Nonnegative int w, @Nonnegative int h) throws IOException {
		
		
		//Leemos la imagen y la devolvemos
		BufferedImage img = ImageIO.read(f);
		System.out.println("Processing: " + f);
		if (img == null)
			return null;
		return img.getScaledInstance(w, h, Image.SCALE_FAST);
	}

	//Funci�n en segundo plano
	@Override
	protected Image doInBackground() throws Exception {
		//Si se encuentra cancelada, devuelve un null
		while(!isCancelled()) {
			
			//Carga la imagen
			Image image = loadScaled(f, width, height);
			//Si es nula comprobaremos en el done
			if (image == null) {
				imagenNula = true;
				return null;
			}
			
			//Devolvemos la imagen
			return image;
			
		}
		return null;
		

		
	

	}

	//Si la imagen se ha cargado, se devuelve al controlador para que la a�ada a la interfaz y actualice la barra de progreso
	@Override
	public void done() {

		try {
			Image imagen = get();
			if (cancelado) {
				controller.setInfo("Se ha cancelado la operaci�n", 100);
			}
			
			else if (imagen == null) {
				controller.setInfo("No pudo cargar " + f.getName(), numImagenes);
			}
			else {
				controller.addImagen(f,get(),numImagenes);
			}
			

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
